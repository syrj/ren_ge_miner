define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'general/mill/index',
                    add_url: 'general/mill/add',
                    edit_url: 'general/mill/edit',
                    del_url: 'general/mill/del',
                    multi_url: 'general/mill/multi',
                    table: 'mill',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'mill_name', title: __('Mill_name')},
                        {field: 'mill_image', title:__('Mill_image'),formatter: Table.api.formatter.image, operate: false},
                        {field: 'is_realname', title: __('Is_realname'), visible:false, searchList: {"0":__('Is_realname 0'),"1":__('Is_realname 1')}},
                        {field: 'is_realname_text', title: __('Is_realname'), operate:false},
                        {field: 'price', title: __('Price')},
                        {field: 'earnings', title: __('Earnings')},
                        {field: 'period', title: __('Period')},
                        {field: 'num', title: __('Num')},
                        {field: 'predict_earnings', title: __('PredictEarnings')},
                        {field: 'status', title: __('Status'), visible:false, searchList: {"0":__('Status 0'),"1":__('Status 1')}},
                        {field: 'status_text', title: __('Status'), operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ],
                showColumns: false,
                showExport: false,
                showToggle: false
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});