define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'deal/index',
                    //add_url: 'deal/add',
                    //edit_url: 'deal/edit',
                    del_url: 'deal/del',
                    //multi_url: 'deal/multi',
                    match_url: 'deal/match',
                    table: 'deal',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'create_time',
                sortOrder: 'desc',
                showToggle: false,
                showColumns: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),visible:false},
                        {field: 'deal_sn', title: __('Deal_sn')},
                        {field: 'user_id', title: __('User_id')},
                        {field: 'user_auth.bank_realname', title: __('Username')},
                        {field: 'user.mobile', title: __('Mobile')},
                        {field: 'cny', title: __('Cny'), operate:'BETWEEN'},
                        {field: 'residue_cny', title: __('Residue_cny'), operate:'BETWEEN'},
                        {field: 'type', title: __('Type'), visible:false, searchList: {"0":__('Type 0'),"1":__('Type 1')}},
                        {field: 'type_text', title: __('Type'), operate:false},
                        {field: 'status', title: __('Status'), visible:false, searchList: {"0":__('Status 0'),"1":__('Status 1'),"2":__('Status 2'),"3":__('Status 3')}},
                        {field: 'status_text', title: __('Status'), operate:false},
                        {field: 'line.bank_realname', title: '顶级推荐人姓名', operate:false},
                        {field: 'line.mobile', title: '顶级推荐人手机号', operate:false},
                        {field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'match_time', title: __('Match_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'remark', title: __('Remark'), operate:false},
                        // {field: 'pay_time', title: __('Pay_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'complete_time', title: __('Complate_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime,visible:false},
                        {field: 'operate', title: __('Operate'), table: table, buttons: [
                                {name: 'match', text: __('Match'), title: '交易匹配', icon: 'fa fa-list', classname: 'btn btn-xs btn-primary btn-dialog', url: 'deal_match/index'}
                            ], /*events: Table.api.events.operate,*/ formatter: Table.api.formatter.buttons}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});