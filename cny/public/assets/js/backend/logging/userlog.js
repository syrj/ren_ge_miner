define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'logging/userlog/index',
                    add_url: 'logging/userlog/add',
                    edit_url: 'logging/userlog/edit',
                    del_url: 'logging/userlog/del',
                    multi_url: 'logging/userlog/multi',
                    table: 'user_account_log',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showToggle: false,
                showColumns: true,
                showExport: false,
                columns: [
                    [
                        /*{checkbox: true},*/
                        {field: 'id', title: __('Id')},
                        {field: 'user_id', title: __('User_id')},
                        {field: 'cny', title: __('Cny'), operate:'BETWEEN'},
                        {field: 'cny_balance', title: __('Cny_balance'), operate:'BETWEEN'},
                        {field: 'sac', title: __('Sac'), operate:'BETWEEN'},
                        {field: 'sac_balance', title: __('Sac_balance'), operate:'BETWEEN'},
                        {field: 'change_time', title: __('Change_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'user.mobile', title: __('User.mobile')},
                        {field: 'desc', title: __('Desc')},
                        {field: 'deal_sn', title: __('Deal_sn')},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});