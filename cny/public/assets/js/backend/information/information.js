define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'information/information/index',
                    add_url: 'information/information/add',
                    edit_url: 'information/information/edit',
                    del_url: 'information/information/del',
                    multi_url: 'information/information/multi',
                    table: 'information',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showToggle: false,
                showColumns: true,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'title', title: __('Title')},
                        {field: 'type', title: __('Type'), visible:false, searchList: {"0":__('Type 0'),"1":__('Type 1'),"2":__('Type 2')}},
                        {field: 'type_text', title: __('Type'), operate:false},
                        {field: 'status', title: __('Status'), formatter:this.enable,visible:true, searchList: {"0":__('Status 0'),"1":__('Status 1')}},
                        // {field: 'status_text', title: __('Status'), operate:false},
                        {field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ],
                showColumns: false,
                showExport: false,
                showToggle: false
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        },
        enable: function (value, row, index) {
            //颜色状态数组,可使用red/yellow/aqua/blue/navy/teal/olive/lime/fuchsia/purple/maroon
            var colorArr = {1: 'success', 0: 'grey', deleted: 'danger', locked: 'info'};
            //如果字段列有定义custom
            if (typeof this.custom !== 'undefined') {
                colorArr = $.extend(colorArr, this.custom);
            }
            value = value === null ? '' : value.toString();
            var color = value && typeof colorArr[value] !== 'undefined' ? colorArr[value] : 'primary';
            var newValue = value.charAt(0).toUpperCase() + value.slice(1);
            var valueText = {1:__('Status 1'),0:__('Status 0')};
            //渲染状态
            var html = '<span class="text-' + color + '"> ' + valueText[newValue] + '</span>';
            if (this.operate != false) {
                html = '<a href="javascript:;" class="searchit" data-toggle="tooltip"  data-field="' + this.field + '" data-value="' + value + '">' + html + '</a>';
            }
            return html;
        },
    };
    return Controller;
});