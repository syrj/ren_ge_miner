define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            var ids = $("#ids").val();
            Table.api.init({
                extend: {
                    index_url: 'deal_match/index/ids/'+ids,
                    match_url: 'deal_match/match',
                    table: 'deal',
                }
            });
            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'cny',
                sortOrder: 'desc',
                showToggle: false,
                showColumns: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: '交易ID'},
                        {field: 'user_id', title: __('User_id')},
                        {field: 'user.bank_realname', title: '姓名'},
                        {field: 'user.mobile', title: '手机号'},
                        {field: 'cny', title: __('Cny'), operate:'BETWEEN',sortable:true,visible:false},
                        {field: 'residue_cny', title: __('Residue_cny'), operate:'BETWEEN',sortable:true},
                        {field: 'type', title: __('Type'), visible:false,/* searchList: {"0":__('Type 0'),"1":__('Type 1')}*/},
                        {field: 'type_text', title: __('Type'), operate:false},
                        {field: 'status', title: __('Status'), visible:false,},
                        {field: 'status_text', title: __('Status'), operate:false},
                        {field: 'line.bank_realname', title: '顶级推荐人姓名', operate:false},
                        {field: 'line.mobile', title: '顶级推荐人手机号', operate:false},
                        {field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'remark', title: '备注', operate:false},
                    ]
                ]
            });


            // 为表格绑定事件
            Table.api.bindevent(table);

            //  监听匹配按钮的点击事件
            $(".btn-match").click(function () {
                var options = table.bootstrapTable('getOptions');
                var did = $("#ids").val();
                var ids ;
                if (options.templateView) {
                    ids = $.map($("input[data-id][name='checkbox']:checked"), function (dom) {
                        return $(dom).data("id");
                    });
                } else {
                    ids = $.map(table.bootstrapTable('getSelections'), function (row) {
                        return row[options.pk];
                    });
                }
                //

                //  请求确认
                Layer.confirm(
                    __('确定要匹配所选的 %s 个项目？', ids.length),
                    {icon: 3, title: __('Warning'), offset: 0, shadeClose: true},
                    function (index) {
                        Layer.close(index); //  关闭对话框
                        ids = ($.isArray(ids) ? ids.join(",") : ids);
                        Fast.api.ajax({
                            url: 'deal_match/deal_confirm',
                            data: {ids: ids, main_id: did}
                        }, function (data, ret) {
                            //成功的回调
                            //alert(ret.msg);
                            setTimeout(function () {
                                Fast.api.close(data);
                                table.bootstrapTable('refresh');
                            }, 1000);
                        });
                    });
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },

        },
    };
    return Controller;
});