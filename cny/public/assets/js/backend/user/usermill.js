define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'user/usermill/index',
                    add_url: 'user/usermill/add',
                    edit_url: 'user/usermill/edit',
                    del_url: 'user/usermill/del',
                    multi_url: 'user/usermill/multi',
                    table: 'user_mill',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'user_id', title: __('User_id')},
                        {field: 'user.nickname', title: __('User.nickname'),operate: false},
                        {field: 'mill_id', title: __('Mill_id')},
                        {field: 'mill.mill_name', title: __('Mill.mill_name'),operate: false},
                        {field: 'earnings', title: __('Earnings'), operate:'BETWEEN'},
                        {field: 'earnings_yet', title: __('Earnings_yet'), operate:'BETWEEN'},
                        {field: 'period', title: __('Period')},
                        {field: 'status', title: __('Status'), formatter: this.enable,searchList: {"0":'未启用',"1":'已启用'}},
                        {field: 'buy_time', title: __('Buy_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'start_time', title: __('Start_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'end_time', title: __('End_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'last_time', title: __('Last_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ],
                showColumns: false,
                showExport: false,
                showToggle: false
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        },
        enable: function (value, row, index) {
            //颜色状态数组,可使用red/yellow/aqua/blue/navy/teal/olive/lime/fuchsia/purple/maroon
            var colorArr = {1: 'success', 0: 'grey', deleted: 'danger', locked: 'info'};
            //如果字段列有定义custom
            if (typeof this.custom !== 'undefined') {
                colorArr = $.extend(colorArr, this.custom);
            }
            value = value === null ? '' : value.toString();
            var color = value && typeof colorArr[value] !== 'undefined' ? colorArr[value] : 'primary';
            var newValue = value.charAt(0).toUpperCase() + value.slice(1);
            var valueText = {1:__('Normal'),0:'未启用'};
            //渲染状态
            var html = '<span class="text-' + color + '"><i class="fa fa-circle"></i> ' + valueText[newValue] + '</span>';
            if (this.operate != false) {
                html = '<a href="javascript:;" class="searchit" data-toggle="tooltip"  data-field="' + this.field + '" data-value="' + value + '">' + html + '</a>';
            }
            return html;
        },
    };
    return Controller;
});