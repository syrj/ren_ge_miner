define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller;
    Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'user/user/index',
                    add_url: 'user/user/add',
                    edit_url: 'user/user/edit',
                    del_url: 'user/user/del',
                    multi_url: 'user/user/multi',
                    table: 'user',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'user.createtime',
                columns: [
                    [
                        {checkbox: true},
                        {title: __('Id'), field: 'id'},
                        {field: 'username', title: __('Username'), operate: 'LIKE'},
                        {field: 'cert.bank_realname', title: __('Realname'), operate: 'LIKE'},
                        {field: 'nickname', title: __('Nickname'), operate: 'LIKE'},
                        {field: 'mobile', title: __('Mobile'), operate: 'LIKE'},
                        {field: 'avatar', title: __('Avatar'), formatter: Table.api.formatter.image, operate: false},
                        {field: 'cny', title: 'CNY',operate: 'LIKE', sortable: true },
                        {field: 'freeze_cny', title: __('Freeze_Cny') ,operate: 'LIKE', sortable: true},
                        {field: 'sac', title: 'SAC' ,operate: 'LIKE', sortable: true},
                        // {field: 'freeze_sac', title: __('Freeze_Sac') ,operate: 'LIKE', sortable: true},
                        {field: 'first', title: __('First'),visible:false},
                        {field: 'second', title: __('Second'),visible:false},
                        {field: 'third', title: __('Third'),visible:false},
                        {field: 'invite1.bank_realname', title: __('FirstName')},
                        {field: 'invite2.bank_realname', title: __('SecondName')},
                        {field: 'invite3.bank_realname', title: __('ThirdName')},
                        {
                            field: 'status',
                            title: __('Status'),
                            formatter: this.enable,
                            searchList: {"normal":__('Normal'),"hidden":__('Freeze')}
                        },
                        {
                            field: 'is_buymill',
                            title: __('Is_BuyMill'),
                            formatter: this.freeze,
                            searchList: {"1":__('Bought'),"0":__('NoBuy')}
                        },
                        {
                            field: 'operate',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ],
                showColumns: false,
                showExport: false,
                showToggle: false
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        },
        enable: function (value, row, index) {
            //颜色状态数组,可使用red/yellow/aqua/blue/navy/teal/olive/lime/fuchsia/purple/maroon
            var colorArr = {'normal': 'success', 'hidden': 'grey', deleted: 'danger', locked: 'info'};
            //如果字段列有定义custom
            if (typeof this.custom !== 'undefined') {
                colorArr = $.extend(colorArr, this.custom);
            }
            value = value === null ? '' : value.toString();
            var color = value && typeof colorArr[value] !== 'undefined' ? colorArr[value] : 'primary';
            var newValue = value.charAt(0).toUpperCase() + value.slice(1);
            console.log(newValue);
            var valueText = {'Normal':__('Normal'),'Hidden':__('Freeze')};
            //渲染状态
            var html = '<span class="text-' + color + '"><i class="fa fa-circle"></i> ' + valueText[newValue] + '</span>';
            if (this.operate != false) {
                html = '<a href="javascript:;" class="searchit" data-toggle="tooltip"  data-field="' + this.field + '" data-value="' + value + '">' + html + '</a>';
            }
            return html;
        },
        freeze: function (value, row, index) {
            //颜色状态数组,可使用red/yellow/aqua/blue/navy/teal/olive/lime/fuchsia/purple/maroon
            var colorArr = {1: 'success', 0: 'grey', deleted: 'danger', locked: 'info'};
            //如果字段列有定义custom
            if (typeof this.custom !== 'undefined') {
                colorArr = $.extend(colorArr, this.custom);
            }
            value = value === null ? '' : value.toString();
            var color = value && typeof colorArr[value] !== 'undefined' ? colorArr[value] : 'primary';
            var newValue = value.charAt(0).toUpperCase() + value.slice(1);
            var valueText = {"1":__('Bought'),"0":__('NoBuy')};
            //渲染状态
            var html = '<span class="text-' + color + '"> ' + valueText[newValue] + '</span>';
            if (this.operate != false) {
                html = '<a href="javascript:;" class="searchit" data-toggle="tooltip"  data-field="' + this.field + '" data-value="' + value + '">' + html + '</a>';
            }
            return html;
        },
        /*freeze: function (value, row, index) {
            //颜色状态数组,可使用red/yellow/aqua/blue/navy/teal/olive/lime/fuchsia/purple/maroon
            var colorArr = {1: 'success', 0: 'grey', deleted: 'danger', locked: 'info'};
            //如果字段列有定义custom
            if (typeof this.custom !== 'undefined') {
                colorArr = $.extend(colorArr, this.custom);
            }
            value = value === null ? '' : value.toString();
            var color = value && typeof colorArr[value] !== 'undefined' ? colorArr[value] : 'primary';
            var newValue = value.charAt(0).toUpperCase() + value.slice(1);
            var valueText = {1:__('CertYes'),0:__('CertNo')};
            //渲染状态
            var html = '<span class="text-' + color + '"> ' + valueText[newValue] + '</span>';
            if (this.operate != false) {
                html = '<a href="javascript:;" class="searchit" data-toggle="tooltip"  data-field="' + this.field + '" data-value="' + value + '">' + html + '</a>';
            }
            return html;
        },*/
    };
    return Controller;
});