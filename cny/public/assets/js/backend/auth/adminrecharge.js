define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'auth/adminrecharge/index',
                    add_url: 'auth/adminrecharge/add',
                    //edit_url: 'auth/adminrecharge/edit',
                    //del_url: 'auth/adminrecharge/del',
                    //multi_url: 'auth/adminrecharge/multi',
                    table: 'admin_recharge_log',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showColumns: false,
                showExport: false,
                showToggle: false,
                columns: [
                    [
                        {checkbox: true,visible:false},
                        {field: 'id', title: __('Id'),visible:false},
                        {field: 'admin_id', title: __('Admin_id')},
                        {field: 'admin_name', title: __('Admin_name')},
                        {field: 'title', title: __('Title')},
                        {field: 'type', title: __('Type'), visible:false, searchList: {"1":__('Type 1'),"2":__('Type 2')}},
                        {field: 'type_text', title: __('Type'), operate:false},
                        {field: 'value', title: __('Value'), operate:'BETWEEN'},
                        {field: 'user_id', title: __('User_id')},
                        {field: 'user_name', title: __('User_name')},
                        {field: 'ip', title: __('Ip')},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
            Table.api.init();
            var show_table = $("#show_table");
            // 初始化表格
            show_table.bootstrapTable({
                url: 'deal_match/search_user_info/ids/0',
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {title: '用户ID', field: 'id', sortable: true},
                        {field: 'username', title: __('Username'),},
                        {field: 'nickname', title: __('Nickname'),},
                        {field: 'mobile', title: '手机号'},
                        {field: 'cny', title: 'CNY' },
                        {field: 'sac', title: 'SAC'},
                    ]
                ],
                showColumns: false,
                showExport: false,
                showToggle: false,
                search:false,
                commonSearch:false

            });
            Table.api.bindevent(show_table);


            $("#c-user_id").keyup(function () {
                    let id = $("#c-user_id").val();
                    //let get_url = id === '' ? 'deal_match/search_user_info/ids/0' : 'deal_match/search_user_info/';
                    let get_url = 'deal_match/search_user_info';
                    $.ajax({
                        type: "post",
                        url:  get_url,
                        data: {ids : id},
                        dataType:"json",
                        success : function(json) {
                            console.log(json);
                            $("#show_table").bootstrapTable('load', json);//主要是要这种写法
                        }
                    });
                }
            );
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        },
    };
    return Controller;
});