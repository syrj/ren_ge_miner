<?php
/**
 * Created by PhpStorm.
 * User: DR
 * Date: 2018/8/3
 * Time: 14:05
 */

namespace app\common\behavior;


use app\common\model\UserRecommend;

class Register
{
    /*
     * 用户注册钩子
     *
     *@param object $reqeust son=下级账号 user=创建者
     */
    public function userRegisterSuccessed(&$request)
    {
        $user   = $request['user'];
        $son    = $request['son'];
        if($user)
        {
           $this->addUserRelation($user,$son); //处理用户表的用户关系
        }else{
            UserRecommend::createFather($son->id);   //后台注册成为顶级用户写入用户推荐关系表
        }
    }

    public function addUserRelation($user,$son)
    {
        $son->first     = $user->id;
        $son->second    = $user->first;
        $son->third     = $user->second;
        $son->save();
        UserRecommend::createSon($user->id,$son->id);
    }

}