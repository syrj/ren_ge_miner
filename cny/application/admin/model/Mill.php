<?php

namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;

class Mill extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    // 表名
    protected $name = 'mill';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [
        'is_realname_text',
        'status_text'
    ];
    

    
    public function getIsRealnameList()
    {
        return ['0' => __('Is_realname 0'),'1' => __('Is_realname 1')];
    }     

    public function getStatusList()
    {
        return ['0' => __('Status 0'),'1' => __('Status 1')];
    }     


    public function getIsRealnameTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['is_realname'];
        $list = $this->getIsRealnameList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStatusTextAttr($value, $data)
    {        
        $value = $value ? $value : $data['status'];
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
