<?php

namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;

class User extends Model
{

    use SoftDelete;
    protected $deleteTime = 'delete_time';
    // 表名
    protected $name = 'user';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    // 追加属性
    protected $append = [
        'prevtime_text',
        'logintime_text',
        'jointime_text'
    ];

    protected static function init()
    {
        self::beforeUpdate(function ($row) {
            $changed = $row->getChangedData();
            //如果有修改密码
            if (isset($changed['password'])) {
                if ($changed['password']) {
                    $row->password = \app\common\library\Auth::instance()->getEncryptPassword($changed['password'], $row->salt);
                } else {
                    unset($row->password);
                }
            }
            if (isset($changed['pay_password'])) {
                if ($changed['pay_password']) {
                    $row->pay_password = \app\common\library\Auth::instance()->getEncryptPassword($changed['pay_password'], $row->salt);
                } else {
                    unset($row->pay_password);
                }
            }
        });
    }

    public function getGenderList()
    {
        return ['1' => __('Male'), '0' => __('Female')];
    }

    public function getStatusList()
    {
        return ['normal' => __('Normal'), 'hidden' => __('Hidden')];
    }

    public function getPrevtimeTextAttr($value, $data)
    {
        $value = $value ? $value : $data['prevtime'];
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    public function getLogintimeTextAttr($value, $data)
    {
        $value = $value ? $value : $data['logintime'];
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    public function getJointimeTextAttr($value, $data)
    {
        $value = $value ? $value : $data['jointime'];
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setPrevtimeAttr($value)
    {
        return $value && !is_numeric($value) ? strtotime($value) : $value;
    }

    protected function setLogintimeAttr($value)
    {
        return $value && !is_numeric($value) ? strtotime($value) : $value;
    }

    protected function setJointimeAttr($value)
    {
        return $value && !is_numeric($value) ? strtotime($value) : $value;
    }

    public function getAvatarAttr($value)
    {
        return $value?$value:'/assets/img/avatar.png';
    }

    public function group()
    {
        return $this->belongsTo('UserGroup', 'group_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }

    public function cert()
    {
        return $this->belongsTo('UserCertification','id' , 'user_id','','LEFT')->setEagerlyType(0);
    }
    public function hcert()
    {
        return $this->hasOne('user_certification','user_id','id');
    }
    public function invite1()
    {
        return $this->hasOne('UserCertification','user_id','first','','LEFT');
    }
    public function invite2()
    {
        return $this->hasOne('UserCertification','user_id','second','','LEFT');
    }
    public function invite3()
    {
        return $this->hasOne('UserCertification','user_id','third','','LEFT');
    }

}
