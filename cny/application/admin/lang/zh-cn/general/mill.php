<?php

return [
    'Mill_name'  =>  '矿机名称',
    'Mill_image'  =>  '矿机图片',
    'Is_realname'  =>  '是否需要实名',
    'Is_realname 0'  =>  '不实名',
    'Is_realname 1'  =>  '实名',
    'Price'  =>  '租赁价格',
    'Earnings'  =>  '收益总量',
    'EarningsYet'  =>  '已产生收益',
    'PredictEarnings'  =>  '预计收益',
    'Period'  =>  '运行周期(天)',
    'Status'  =>  '状态',
    'Status 0'  =>  '不启用',
    'Status 1'  =>  '启用',
    'Createtime'  =>  '创建时间',
    'Data'  =>  '描述',
    'Num'   =>  '可购买数量',
];
