<?php

return [
    'Id'  =>  'ID',
    'Title'  =>  '标题',
    'Type'  =>  '类型',
    'Type 0'  =>  '联系客服',
    'Type 1'  =>  '关于我们',
    'Type 2'  =>  '注册协议',
    'Status'  =>  ' 状态',
    'Status 0'  =>  '隐藏',
    'Status 1'  =>  '显示',
    'Content'  =>  '内容',
    'Create_time'  =>  '创建时间'
];
