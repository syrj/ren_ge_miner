<?php

return [
    'Id'  =>  'ID',
    'User_id'  =>  '用户ID',
    'Mill_id'  =>  '矿机ID',
    'Earnings'  =>  '应获总收益',
    'Earnings_yet'  =>  '已获收益',
    'Period'  =>  '已运行(天)',
    'Status'  =>  '状态',
    'Status 0'  =>  '不启用',
    'Status 1'  =>  '启用',
    'Buy_time'  =>  '购买时间',
    'Start_time'  =>  '启用时间',
    'Last_time'  =>  '上次返币时间',
    'End_time'  =>  '到期时间',
    'User.nickname'  =>  '昵称',
    'Mill.mill_name'  =>  '矿机名称'
];
