<?php

return [
    'Id'  =>  'ID',
    'Order_sn'  =>  '订单号',
    'Buy_deal_sn'  =>  '买入订单号',
    'Buy_user_id'  =>  '买家ID',
    'Sale_deal_sn'  =>  '卖出订单号',
    'Sale_user_id'  =>  '卖家ID',
    'Create_time'  =>  '撮合时间',
    'Pay_time'  =>  '付款时间',
    'Complete_time'  =>  '完成时间',
    'Type'  =>  '买卖类型',
    'Type 0'  =>  '买入为主',
    'Type 1'  =>  '卖出为主',
    'Status'  =>  '订单状态',
    'Status 1'  =>  '交易中',
    'Status 2'  =>  '已付款',
    'Status 3'  =>  '已完成',
    'Cny'  =>  '总交易cny',
    'BuyRemark'  =>  '买家留言',
    'SoldRemark'  =>  '卖家留言'
];
