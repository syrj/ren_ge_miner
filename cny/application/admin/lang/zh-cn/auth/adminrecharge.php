<?php

return [
    'Recharge'  =>  '充值',
    'Admin_id'  =>  '管理员ID',
    'Admin_name'  =>  '管理员名字',
    'Title'  =>  '日志标题',
    'Type'  =>  '充值类型',
    'Type 1'  =>  'CNY',
    'Type 2'  =>  'SAC',
    'Value'  =>  '充值面额',
    'User_id'  =>  '充值用户ID',
    'User_name'  =>  '充值用户姓名',
    'Ip'  =>  'IP',
    'Createtime'  =>  '操作时间'
];
