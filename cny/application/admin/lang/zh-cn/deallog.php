<?php

return [
    'Id'  =>  'ID',
    'Deal_sn'  =>  '买卖订单号',
    'User_id'  =>  '用户ID',
    'Cny'  =>  'CNY',
    'Residue_cny'  =>  '未交易CNY',
    'Sac_price'  =>  '交易金额(CNY)',
    'Sac_num'  =>  'SAC数量',
    'Type'  =>  '交易类型',
    'Type 0'  =>  '买入',
    'Type 1'  =>  '卖出',
    'Status'  =>  '交易状态',
    'Status 0'  =>  '取消交易',
    'Status 1'  =>  '未交易',
    'Status 2'  =>  '交易中',
    'Status 3'  =>  '交易完成',
    'Pay_status'  =>  '支付状态',
    'Pay_status 0'  =>  '未支付',
    'Pay_status 1'  =>  '未收款',
    'Pay_status 2'  =>  '交易完成',
    'Create_time'  =>  '创建订单时间',
    'Match_time'  =>  '撮合时间',
    'Pay_time'  =>  '交易时间',
    'Complate_time'  =>  '完成时间'
];
