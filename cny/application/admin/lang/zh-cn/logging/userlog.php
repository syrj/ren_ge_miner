<?php

return [
    'Id'  =>  'ID',
    'User_id'  =>  '用户ID',
    'Cny'  =>  '收支CNY',
    'Cny_balance'  =>  'CNY结余',
    'Sac'  =>  '收支SAC',
    'Sac_balance'  =>  'sac结余',
    'Cny_freeze'  =>  '冻结CNY',
    'Change_time'  =>  '变动时间',
    'Order_sn'  =>  '订单编号',
    'Deal_sn'  =>  '交易编号',
    'Desc'  =>  '描述',
    'User.mobile'  =>  '手机号'
];
