<?php

return [
    'Id'  =>  'ID',
    'User_id'  =>  '用户ID',
    'Images'  =>  '图片',
    'Content'  =>  '内容',
    'Create_time'  =>  '反馈时间',
    'Reply_images'  =>  '回复图片',
    'Reply'  =>  '回复',
    'Reply_time'  =>  '回复时间',
    'Status'  =>  '回复状态',
    'Status 0'  =>  '未回复',
    'Status 1'  =>  '已回复',
    'Reply_id'  =>  '回复人id',
    'User.mobile'  =>  '手机号'
];
