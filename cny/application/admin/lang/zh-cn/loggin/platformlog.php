<?php

return [
    'Id'  =>  'ID',
    'User_id'  =>  '用户ID',
    'Cny'  =>  'CNY',
    'Desc'  =>  '描述',
    'Create_time'  =>  '创建时间',
    'Deal_sn'  =>  '挂单号',
    'User.mobile'  =>  '手机号'
];
