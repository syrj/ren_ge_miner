<?php

namespace app\admin\validate;

use think\Validate;

class AdminRechargeLog extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'type'      =>  'require|in:1,2',
        'value'     =>  'require|number',
        'user_id'   =>  'require|number',
    ];
    /**
     * 提示消息
     */
    protected $message = [
        'type.require'      =>  '充值类型不能为空',
        'value.require'     =>  '充值面额不能为空',
        'value.number'      =>  '充值面额有误',
        'user_id.require'   =>  '用户ID错误',
    ];
    /**
     * 验证场景
     */
    protected $scene = [
        'add'  => ['type', 'value', 'user_id'],
        'edit' => [],
    ];
    
}
