<?php

namespace app\admin\controller;

use app\common\controller\Backend;

/**
 * 挂单管理
 *
 * @icon fa fa-circle-o
 */
class Deallog extends Backend
{
    
    /**
     * Deal模型对象
     * @var \app\admin\model\Deal
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('Deal');
        $this->view->assign("typeList", $this->model->getTypeList());
        $this->view->assign("statusList", $this->model->getStatusList());
        $this->view->assign("payStatusList", $this->model->getPayStatusList());
    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */


    /**
     * 查看
     */
    public function index()
    {
        \think\Config::load(APP_PATH.'api/config.php');
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $log_status = [
                config('DealStatus.Cancel'),config('DealStatus.Finished')
            ];
            $my_where = [
                'status'    =>  ['in' , $log_status]
            ];
            /*$my_where_or = [
                'residue_cny'   =>  ['<=' , 0],
            ];*/
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->where($my_where)
                ->where($where)
                //->whereOr($my_where_or)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($my_where)
                ->where($where)
                //->whereOr($my_where_or)
                ->order($sort, $order)
                //->order('status','desc')
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }


    /**
     * 删除
     */
    public function del($ids = "")
    {

        $this->error('删除功能下线');
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds)) {
                $count = $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->select();
            $count = 0;
            foreach ($list as $k => $v) {
                $count += $v->delete();
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }
}
