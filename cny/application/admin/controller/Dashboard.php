<?php

namespace app\admin\controller;


use app\common\controller\Backend;
use think\Config;
use think\Db;

/**
 * 控制台
 *
 * @icon fa fa-dashboard
 * @remark 用于展示当前系统中的统计数据、统计报表及重要实时数据
 */
class Dashboard extends Backend
{

    /**
     * 查看
     */
    public function index()
    {
        $seventtime = \fast\Date::unixtime('day', -7);
        $sevensignup = Db::name('user')->where('createtime','between',"$seventtime,".time())->count();
        $sevenlogin = Db::name('user')->where('logintime','between',"$seventtime,".time())->count();
        $paylist = $createlist = [];
        for ($i = 0; $i < 7; $i++)
        {
            $day_time = $seventtime + ($i * 86400);
            $day_time_plus = $day_time + 86400;
            $day = date("Y-m-d", $day_time);
            $createlist[$day] = $this->DayCreateDeal($day_time,$day_time_plus);
            $paylist[$day] = $this->DayFinishDeal($day_time,$day_time_plus);
        }
        $hooks = config('addons.hooks');
        $uploadmode = isset($hooks['upload_config_init']) && $hooks['upload_config_init'] ? implode(',', $hooks['upload_config_init']) : 'local';
        $addonComposerCfg = ROOT_PATH . '/vendor/karsonzhang/fastadmin-addons/composer.json';
        Config::parse($addonComposerCfg, "json", "composer");
        $config = Config::get("composer");
        $addonVersion = isset($config['version']) ? $config['version'] : __('Unknown');
        $totaluser = Db::name('user')->count();
        $totaldeal = Db::name('deal')->count();
        $today = \fast\Date::unixtime('day',-1);
        $todayuserlogin = Db::name('user')->where('logintime','between',"$today,".time())->count();
        $todayusersignup = Db::name('user')->where('createtime','between',"$today,".time())->count();
        $todaydeal = Db::name('deal')->where('create_time','between',"$today,".time())->count();
        $unsettleorder = Db::name('deal')->where('status','=',1)->count();
        $sevendnu = round($todayusersignup/$sevensignup,2)*100;
        $sevendau = round($todayuserlogin/$sevenlogin,2)*100;
        $this->view->assign([
            'totaluser'        => $totaluser,
            'totalviews'       => $todayusersignup,
            'totalorder'       => $totaldeal,
            'totalorderamount' => $todaydeal,
            'todayuserlogin'   => $todayuserlogin,
            'todayusersignup'  => $todayusersignup,
            'todayorder'       => $todaydeal,
            'unsettleorder'    => $unsettleorder,
            'sevendnu'         => $sevendnu.'%',
            'sevendau'         => $sevendau.'%',
            'paylist'          => $paylist,
            'createlist'       => $createlist,
            'addonversion'       => $addonVersion,
            'uploadmode'       => $uploadmode
        ]);

        return $this->view->fetch();
    }
    private function DayCreateDeal($day_time,$day_time_plus)
    {
        return Db::name('deal')->where('create_time','between',[$day_time,$day_time_plus])->count('id');
    }
    private function DayFinishDeal($day_time,$day_time_plus)
    {
        return Db::name('deal')
            ->where('create_time','between',[$day_time,$day_time_plus])
            ->where('status',3)
            ->count('id');
    }

}
