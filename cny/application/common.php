<?php

// 公共助手函数


if (!function_exists('send_matched_sms'))
{
    /**
     * 订单状态改变发送验证码
     * @param $mobile string 手机号
     * @param $order_sn string 交易号
     * @param string $status 状态，默认为匹配完成
     * @return array|mixed
     */
    function send_matched_sms($mobile,$order_sn,$status = '匹配完成')
    {
        require_once (ROOT_PATH . 'vendor/smssdk/SmsApi.php');
        $time = '12小时';
        $content = "尊敬的用户：您的订单（" . $order_sn . "）已". $status .",订单有效期为" . $time . "，请尽快处理。".date('Y年m月d日H时i分');
        $A_Sms= new \SmsApi();
        $res = $A_Sms->inner_send($mobile,$content);
        //trace(['订单短信','mobile'=>$mobile,'content'=>$content,'time'=>date('Y-m-d H:i:s')],'sms');
        if ($res['status'] == 1)
            return true;
        return $res['info'];
    }
}
if (!function_exists('send_common_sms'))
{
    /**
     * @param $mobile string  订单完成发送验证码
     * @param $order_sn string 订单交易号
     * @param string $status   订单状态（已交易完成）
     * @return bool
     */
    function send_common_sms($mobile,$order_sn,$status = '已交易完成')
    {
        require_once (ROOT_PATH . 'vendor/smssdk/SmsApi.php');
        $content = "尊敬的用户：您的订单（" . $order_sn . "）".$status;
        $A_Sms= new \SmsApi();
        $res = $A_Sms->inner_send($mobile,$content);
        //trace(['订单短信','mobile'=>$mobile,'content'=>$content,'time'=>date('Y-m-d H:i:s')],'sms');
        if ($res['status'] == 1)
            return true;
        return $res['info'];
    }
}


if (!function_exists('account_log')) {
    /**
     * 记录用户资金流动
     * @param $user_id string 用户ID
     * @param $cny   double   收支CNY
     * @param $cny_balance  double CNYj结余
     * @param $sac    double  收支SAC
     * @param $sac_balance double SACj结余
     * @param $desc   string  描述
     * @param $cny_freeze string 冻结CNY
     * @param $deal_sn string 挂单号
     * @param $type string 1=正常交易 2=奖励
     * @param $order_sn string 订单编号
     */


    function account_log($user_id, $cny, $cny_balance, $sac, $sac_balance, $desc,$cny_freeze = '', $deal_sn = '', $order_sn = '',$type='1')
    {
        $data = [
            'user_id' => $user_id,
            'cny' => $cny,
            'cny_balance' => $cny_balance,
            'sac' => $sac,
            'sac_balance' => $sac_balance,
            'desc' => $desc,
            'cny_freeze' => $cny_freeze,
            'deal_sn' => $deal_sn,
            'order_sn' => $order_sn,
            'type' => $type,
            'change_time' => time(),
        ];
        \think\Db::name('user_account_log')->insert($data);
    }
}

if (!function_exists('platform_log')) {
    /*
     * 记录平台资金流动
     * @param $user_id  用户ID
     * @param $cny CNY
     * @param $desc 描述
     * @param $deal_sn  挂单编号
     */
    function platform_log($user_id, $cny, $desc, $deal_sn = '')
    {
        $data = [
            'user_id' => $user_id,
            'cny' => $cny,
            'desc' => $desc,
            'deal_sn' => $deal_sn,
            'create_time' => time(),
        ];
        \think\Db::name('platform_log')->insert($data);
    }
}


if (!function_exists('generate_order_id')) {
    /**
     * 根据 用户ID 生成订单号
     * @param $user_id  int 用户ID
     * @return string   唯一订单号
     */
    function generate_order_id($user_id)
    {
        return date('YmdHis') . \fast\Random::numeric(6) . abs(1000000 - $user_id);   //  生成随机订单
    }

}


if (!function_exists('generate_deal_id')) {
    /**
     * 根据 价格或者数量 生成交易流水号
     * @param $price double 订单成交价格/成交量
     * @param int $len 尾号长度
     * @return string   唯一交易号
     */
    function generate_deal_id($price, $len = 5)
    {
        $price = intval($price);
        if (strlen($price) > $len) {
            $price = substr($price, 0, $len);
        } else {
            $price = \fast\Random::numeric($len - strlen($price)) . $price;
        }
        return date('Ymd') . $price . \fast\Random::numeric(6);
    }
}


if (!function_exists('__')) {

    /**
     * 获取语言变量值
     * @param string $name 语言变量名
     * @param array $vars 动态变量值
     * @param string $lang 语言
     * @return mixed
     */
    function __($name, $vars = [], $lang = '')
    {
        if (is_numeric($name) || !$name)
            return $name;
        if (!is_array($vars)) {
            $vars = func_get_args();
            array_shift($vars);
            $lang = '';
        }
        return \think\Lang::get($name, $vars, $lang);
    }

}

if (!function_exists('format_bytes')) {

    /**
     * 将字节转换为可读文本
     * @param int $size 大小
     * @param string $delimiter 分隔符
     * @return string
     */
    function format_bytes($size, $delimiter = '')
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
        for ($i = 0; $size >= 1024 && $i < 6; $i++)
            $size /= 1024;
        return round($size, 2) . $delimiter . $units[$i];
    }

}

if (!function_exists('datetime')) {

    /**
     * 将时间戳转换为日期时间
     * @param int $time 时间戳
     * @param string $format 日期时间格式
     * @return string
     */
    function datetime($time, $format = 'Y-m-d H:i:s')
    {
        $time = is_numeric($time) ? $time : strtotime($time);
        return date($format, $time);
    }

}

if (!function_exists('human_date')) {

    /**
     * 获取语义化时间
     * @param int $time 时间
     * @param int $local 本地时间
     * @return string
     */
    function human_date($time, $local = null)
    {
        return \fast\Date::human($time, $local);
    }

}

if (!function_exists('cdnurl')) {

    /**
     * 获取上传资源的CDN的地址
     * @param string $url 资源相对地址
     * @param boolean $domain 是否显示域名 或者直接传入域名
     * @return string
     */
    function cdnurl($url, $domain = false)
    {
        $url = preg_match("/^https?:\/\/(.*)/i", $url) ? $url : \think\Config::get('upload.cdnurl') . $url;
        if ($domain && !preg_match("/^(http:\/\/|https:\/\/)/i", $url)) {
            if (is_bool($domain)) {
                $public = \think\Config::get('view_replace_str.__PUBLIC__');
                $url = rtrim($public, '/') . $url;
                if (!preg_match("/^(http:\/\/|https:\/\/)/i", $url)) {
                    $url = request()->domain() . $url;
                }
            } else {
                $url = $domain . $url;
            }
        }
        return $url;
    }

}


if (!function_exists('is_really_writable')) {

    /**
     * 判断文件或文件夹是否可写
     * @param    string $file 文件或目录
     * @return    bool
     */
    function is_really_writable($file)
    {
        if (DIRECTORY_SEPARATOR === '/') {
            return is_writable($file);
        }
        if (is_dir($file)) {
            $file = rtrim($file, '/') . '/' . md5(mt_rand());
            if (($fp = @fopen($file, 'ab')) === FALSE) {
                return FALSE;
            }
            fclose($fp);
            @chmod($file, 0777);
            @unlink($file);
            return TRUE;
        } elseif (!is_file($file) OR ($fp = @fopen($file, 'ab')) === FALSE) {
            return FALSE;
        }
        fclose($fp);
        return TRUE;
    }

}

if (!function_exists('rmdirs')) {

    /**
     * 删除文件夹
     * @param string $dirname 目录
     * @param bool $withself 是否删除自身
     * @return boolean
     */
    function rmdirs($dirname, $withself = true)
    {
        if (!is_dir($dirname))
            return false;
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dirname, RecursiveDirectoryIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST
        );

        foreach ($files as $fileinfo) {
            $todo = ($fileinfo->isDir() ? 'rmdir' : 'unlink');
            $todo($fileinfo->getRealPath());
        }
        if ($withself) {
            @rmdir($dirname);
        }
        return true;
    }

}

if (!function_exists('copydirs')) {

    /**
     * 复制文件夹
     * @param string $source 源文件夹
     * @param string $dest 目标文件夹
     */
    function copydirs($source, $dest)
    {
        if (!is_dir($dest)) {
            mkdir($dest, 0755, true);
        }
        foreach (
            $iterator = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($source, RecursiveDirectoryIterator::SKIP_DOTS), RecursiveIteratorIterator::SELF_FIRST) as $item
        ) {
            if ($item->isDir()) {
                $sontDir = $dest . DS . $iterator->getSubPathName();
                if (!is_dir($sontDir)) {
                    mkdir($sontDir, 0755, true);
                }
            } else {
                copy($item, $dest . DS . $iterator->getSubPathName());
            }
        }
    }

}

if (!function_exists('mb_ucfirst')) {

    function mb_ucfirst($string)
    {
        return mb_strtoupper(mb_substr($string, 0, 1)) . mb_strtolower(mb_substr($string, 1));
    }

}

if (!function_exists('addtion')) {

    /**
     * 附加关联字段数据
     * @param array $items 数据列表
     * @param mixed $fields 渲染的来源字段
     * @return array
     */
    function addtion($items, $fields)
    {
        if (!$items || !$fields)
            return $items;
        $fieldsArr = [];
        if (!is_array($fields)) {
            $arr = explode(',', $fields);
            foreach ($arr as $k => $v) {
                $fieldsArr[$v] = ['field' => $v];
            }
        } else {
            foreach ($fields as $k => $v) {
                if (is_array($v)) {
                    $v['field'] = isset($v['field']) ? $v['field'] : $k;
                } else {
                    $v = ['field' => $v];
                }
                $fieldsArr[$v['field']] = $v;
            }
        }
        foreach ($fieldsArr as $k => &$v) {
            $v = is_array($v) ? $v : ['field' => $v];
            $v['display'] = isset($v['display']) ? $v['display'] : str_replace(['_ids', '_id'], ['_names', '_name'], $v['field']);
            $v['primary'] = isset($v['primary']) ? $v['primary'] : '';
            $v['column'] = isset($v['column']) ? $v['column'] : 'name';
            $v['model'] = isset($v['model']) ? $v['model'] : '';
            $v['table'] = isset($v['table']) ? $v['table'] : '';
            $v['name'] = isset($v['name']) ? $v['name'] : str_replace(['_ids', '_id'], '', $v['field']);
        }
        unset($v);
        $ids = [];
        $fields = array_keys($fieldsArr);
        foreach ($items as $k => $v) {
            foreach ($fields as $m => $n) {
                if (isset($v[$n])) {
                    $ids[$n] = array_merge(isset($ids[$n]) && is_array($ids[$n]) ? $ids[$n] : [], explode(',', $v[$n]));
                }
            }
        }
        $result = [];
        foreach ($fieldsArr as $k => $v) {
            if ($v['model']) {
                $model = new $v['model'];
            } else {
                $model = $v['name'] ? \think\Db::name($v['name']) : \think\Db::table($v['table']);
            }
            $primary = $v['primary'] ? $v['primary'] : $model->getPk();
            $result[$v['field']] = $model->where($primary, 'in', $ids[$v['field']])->column("{$primary},{$v['column']}");
        }

        foreach ($items as $k => &$v) {
            foreach ($fields as $m => $n) {
                if (isset($v[$n])) {
                    $curr = array_flip(explode(',', $v[$n]));

                    $v[$fieldsArr[$n]['display']] = implode(',', array_intersect_key($result[$n], $curr));
                }
            }
        }
        return $items;
    }

}

if (!function_exists('var_export_short')) {

    /**
     * 返回打印数组结构
     * @param string $var 数组
     * @param string $indent 缩进字符
     * @return string
     */
    function var_export_short($var, $indent = "")
    {
        switch (gettype($var)) {
            case "string":
                return '"' . addcslashes($var, "\\\$\"\r\n\t\v\f") . '"';
            case "array":
                $indexed = array_keys($var) === range(0, count($var) - 1);
                $r = [];
                foreach ($var as $key => $value) {
                    $r[] = "$indent    "
                        . ($indexed ? "" : var_export_short($key) . " => ")
                        . var_export_short($value, "$indent    ");
                }
                return "[\n" . implode(",\n", $r) . "\n" . $indent . "]";
            case "boolean":
                return $var ? "TRUE" : "FALSE";
            default:
                return var_export($var, TRUE);
        }
    }

}