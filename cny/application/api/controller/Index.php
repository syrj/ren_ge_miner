<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\model\Config;
use fast\Http;
use app\api\model\Deal;

/**
 * 首页接口
 */
class Index extends Api
{

    protected $noNeedLogin = ['getToken','userid','test'];
    protected $noNeedRight = ['*'];
    protected $failException = true;



    /**
     * 首页
     *
     */
    public function index()
    {
        $time = $this->request->request('time');
        $date = '';
        $now = time();
        switch ($time)
        {
            case 'today':
                $date = '/'. ($now - 24*3600) .'/'.time();
                break;
            case 'week':
                $date = '/'. ($now - 7*24*3600) .'/'.time();
                break;
            case 'month':
                $date = '/'. ($now - 30*24*3600) .'/'.time();
                break;
            case 'three_month':
                $date = '/'. ($now - 30*3*24*3600) .'/'.time();
                break;
            case 'year':
                $date = '/'. ($now - 365*24*3600) .'/'.time();
                break;
            case 'all':
                $date = '/'. 1530023419000 .'/'.time();
                break;
        }
        $user = $this->auth->getUser();
        $data = Http::get(config('sac_url').$date);
        $data = json_decode($data, true);
        $price = end($data['price_usd'])[1];        //比特币
        $volume = end($data['volume_usd'])[1];
        $market = end($data['market_cap_usd'])[1];
        $rate = floatval(config('site.rate'));
//        $this->amplitude($price,$data['price_usd'][0]);
//        exit;
        $data = [
            'cny' => $user->cny,
            'sac' => $user->sac,
            'price' => $price,
            'volume' => $volume * $rate,
            'market' => $market * $rate,
            'rate'  => $rate,
            'Kline' => $data,
        ];
        $this->success('success', $data);
    }

//    public function amplitude($now,$yesterday)
//    {
//        echo $now;
//        halt($yesterday);
//        $reach = ($now - $yesterday);
//        return $reach;
//    }
//
//    private function getMillisecond() {
//        list($t1, $t2) = explode(' ', microtime());
//        return (float)sprintf('%.0f',(floatval($t1)+floatval($t2))*1000);
//    }
    /*
     * 获取全部交易
     * @param int $type  买入还是卖出 0 = 买入 1 = 卖出
     */
    public function allDeal()
    {
        $data = $this->request->request();
        $rule = [
            'type' => 'require|in:0,1',
            'listRows' => 'require|integer',
            'page' => 'require|integer',
        ];
        $this->validate($data, $rule);
        $where['type'] = $data['type'];
        $where['residue_cny'] = ['>',0];
        $dealModel = new Deal();
        $dealList = $dealModel->where($where)
            ->cache(true,60)
            ->paginate($data['listRows'], false, ['page' => $data['page']]);
        if ($dealList) {
            $this->success('success', $dealList);
        } else {
            $this->error('error');
        }
    }

    /**
     * 获取token
     */
    public function getToken()
    {
        /*$uid = $this->request->request('uid');
        if (!$uid)return;
        if ($this->auth->direct($uid))
        {
            $user = $this->auth->getUserinfo();
            $this->success('用户id为'.$uid.'的token',$user);
        }*/
    }

    public function test()
    {
        echo \fast\Date::unixtime('day', -3);
        //$this->success('',send_matched_sms(13672654416,'123458548548','匹配完成'));
        /*$sms = new Sms();
        $this->success($sms->check());*/
    }

//   public function userid(){
//
//       for($i = 0 ;$i<1000;$i++){
//           $user_id = Random::nozero(6);
//           while (db('id')->where(['user_id'=>$user_id])->find())
//           {
//               $user_id = Random::nozero(6);
//           }
//           db('id')->insert(['user_id'=>$user_id]);
////           $this->userid();
//
//       }
//
//   }
}
