<?php
/**
 * Created by PhpStorm.
 * User: DR
 * Date: 2018/8/9
 * Time: 9:37
 */

namespace app\api\controller;


use app\common\controller\Api;
use app\api\model\Service as ServiceModel;

/*
 * 客服接口
 * */
class Service extends Api
{
    protected $noNeedLogin = [];
    protected $noNeedRight = '*';
    protected $failException = true;
    /*
     * 发送问题
     * */
    public function sendQuestion()
    {
        $data = $this->request->request();
        if(!isset($data['images']) && !isset($data['content']))
        {
            $this->error('信息为空!');
        }
        $user = $this->auth->getUser();
        ServiceModel::send($user,$data);
        $this->success('反馈成功');
    }

    /**
     *我的反馈
     */
    public function myQuestions()
    {
        $data = $this->request->request();
        $rule = [
            'listRows' => 'require|integer',
            'page' => 'require|integer',
        ];
        $this->validate($data, $rule);
        $user = $this->auth->getUser();
        $serviceModel = new ServiceModel();
        $list = $serviceModel
            ->where('user_id',$user->id)
            ->order('create_time ASC')
            ->paginate($data['listRows'], false, ['page' => $data['page']])
            ->toArray();
        $list['avatar'] = $user->avatar;
        $this->success('success',$list);
    }
}