<?php
/**
 * Created by PhpStorm.
 * User: DR
 * Date: 2018/8/5
 * Time: 11:01
 */

namespace app\api\controller;


use app\api\model\Order;
use app\api\model\UserAccountLog;
use app\common\controller\Api;
use app\api\model\Deal;
use think\Db;

/*
 * 获取交易记录信息接口
 */

class Logging extends Api
{
    protected $noNeedLogin = [];
    protected $noNeedRight = '*';
    protected $failException = true;

    /*
     * 获取财务日志
     * @param string $token 用户token
     * @param string $type 币种 cny,sac
     * @param int $status 0=支出 1=收入 2 =全部
     * @param int $listRows 每页条数
     * @param int $page 页数
     */
    public function financialLog()
    {
        $data = $this->request->request();
        $rule = [
            'type' => 'require',
            'status' => 'require|in:0,1,2,3',
            'listRows' => 'require|integer',
            'page' => 'require|integer',
        ];
        $this->validate($data, $rule);
        $field = 'change_time,desc,type';
        $type = $data['type'];
        if ($type == 'cny') {
            $field .= ',cny';
        } elseif ($type == 'sac') {
            $field .= ',sac';
        }
        $user = $this->auth->getUser();
        $where = 'user_id = ' . $user->id;
        //  最近三天的记录
        $where .= ' and change_time > '.\fast\Date::unixtime('day', -5);
        /*$befor = time();
        $after = time()-(3*24*3600);
        $where .= ' and change_time between '.$after .' and ' . $befor;*/
        if ($data['status'] == 2) {
            $where .= ' and ' . "$type" . ' <> 0';
        } else {
            if ($data['status'] == 3) {
                $where .= ' and type = 2 and ' . "$type " . '> 0 ';
            } else {
                $status = $data['status'] ? '>' : '<';
                $where .= ' and ' . "$type" . ' ' . $status . ' 0  and type = 1';
            }
        }


        $log = new UserAccountLog();
        $list = $log->where($where)
            ->field($field)
            ->order('change_time desc')
            ->paginate($data['listRows'], false, ['page' => $data['page']]);
        if ($list) {
            $this->success('success', $list);
        } else {
            $this->error('error');
        }
    }

    /*
     * 我的挂单记录
     * @param int $type 0=买入1=卖出
     * @param int $status 0=取消交易,1=未交易,2=交易中,3=交易完成,4=全部
     */
    public function myDealLog()
    {
        $data = $this->request->request();
        $rule = [
            'type' => 'require|in:0,1',
            'listRows' => 'require|integer',
            'page' => 'require|integer',
            'status' => 'require|in:0,1,2,3,4'
        ];
        $this->validate($data, $rule);
        $user = $this->auth->getUser();
        $where['user_id'] = $user->id;
        $where['type'] = $data['type'];
        if ($data['status'] == 4) {
            $where['status'] = ['in', '0,1,2,3'];
        } else {
            $where['status'] = $data['status'];
        }
        $log = new Deal();
        $list = $log->where($where)
            ->order('create_time DESC')
            ->paginate($data['listRows'], false, ['page' => $data['page']]);
        $this->success('success', $list);
    }


//    public function my

    /*
     * 查看挂单情况
     * @param string $deal_sn 挂单号
     * @param int $type 0=买入,1=卖出
     */
    public function myOrder()
    {
        $data = $this->request->request();
        $rule = [
            'deal_sn' => 'require',
            'type' => 'require|in:0,1',
        ];
        $this->validate($data, $rule);
        $type = $data['type'];
//        if($type)
//        {
//            $where['sale_deal_sn'] = $data['deal_sn'];
////            $condi = $type == 1 ? 'buy_deal_sn' : 'sale_deal_sn';
//        }else{
//            $where['buy_deal_sn'] = $data['deal_sn'];
////            $condi = $type == 0 ? 'sale_deal_sn' : 'buy_deal_sn';
//        }
        $orderModel = new Order();
        $order = $orderModel
//            ->where('status',1)
            ->whereOr('buy_deal_sn', $data['deal_sn'])
            ->whereOr('sale_deal_sn', $data['deal_sn'])
            ->select();

//        $order = new Order();
//        $result = $order
//            ->alias('o')
//            ->field('o.order_sn,d.cny,d.create_time,d.type,o.status,o.buy_deal_sn,o.sale_deal_sn,d.user_id')
//            ->join('deal d','d.deal_sn = o.'.$condi,'left')
//            ->where($where)
//            ->select();
//        $status = [
//            1 => '交易中',
//            2 => '已付款',
//            3 => '已完成'
//        ];
//        foreach($result as &$value)
//        {
//            $value['status'] = $status[$value['status']];
//        }
        $list = array();
////        //        $order[0]['type']
        foreach ($order as $value) {
            if ($data['type'] == '1') {
                $map['deal_sn'] = $value['buy_deal_sn'];
            } else {
                $map['deal_sn'] = $value['sale_deal_sn'];
            }
            $deal = Deal::get($map);
            $deal['order_sn'] = $value['order_sn'];
            $deal['buy_deal_sn'] = $value['buy_deal_sn'];
            $deal['sale_deal_sn'] = $value['sale_deal_sn'];
            $deal['order_status'] = $value['status'];
            if($type == 1){
                $deal['buy_remark'] = $value['buy_remark'];
            }else{
                $deal['buy_remark'] = '';
            }
            $deal['pay_times'] = $value['pay_time'];
            $deal['user'] = \app\admin\model\UserCertification::get(['user_id'=>$deal->user_id]);
            $deal['user']['mobile'] = Db::name('user')->where('id','=',$deal->user_id)->find()['mobile'];
            $deal['cny'] = $value['cny'];
            unset($deal['status']);
            array_push($list, $deal);
        }
        $this->success('success', $list);
    }

}