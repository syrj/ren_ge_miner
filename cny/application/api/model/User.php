<?php
/**
 * Created by PhpStorm.
 * User: DR
 * Date: 2018/8/2
 * Time: 16:34
 */

namespace app\api\model;



class User  extends BaseModel
{
    protected $visible = ['id','nickname','avatar'];

    //转账cny
    /*
     * @param object $user  转账人
     * @param object $transfer收账人
     * @param int $cny
     */
    public static function targeCny($user,$transfer,$cny)
    {
        parent::dealUserMoney($user,$cny*-1);
        $desc = '给ID:' . $transfer->id . '转账CNY:'. $cny;
        account_log($user->id,$cny * -1,$user->cny,'',$user->sac,$desc);
        $transfer->cny = $transfer->cny + $cny;
        $transfer->total_cny += $cny;
        if($transfer->save())
        {
            $desc = '收到ID:' . $user->id . '转账CNY:' . $cny;
            account_log($transfer->id,$cny,$transfer->cny,'',$user->sac,$desc);
        }
    }


    //sac兑换cny
    /*
     * @param object $user 用户
     * @param array $data sac价格和数量
     */
    public static function exchangeSac($user,$data)
    {
        $cny = round($data['sacnum'] * $data['sacprice'],4);
        $user->sac = $user->sac - $data['sacnum'];
        $user->cny = $user->cny + $cny;
        $user->save();
        $desc = 'SAC:'.$data['sacnum'].'兑换CNY:'.$cny;
        account_log($user->id,$cny,$user->cny,$data['sacnum']*-1,$user->sac,$desc);
        platform_log($user->id,$cny*-1,$desc);
    }

    //获取直推团队
    public static function getMyTeam($user_id,$listRows,$page)
    {
        $users = self::where('first',$user_id)
            ->paginate($listRows,false,[
                'page' => $page,
            ]);
        return $users;
    }

    //获取三级总人数和
    public static function getMyTeamCount($user_id)
    {
        /*$where = [
            'u.id'  =>   $user_id,
            'us.is_buymill' =>   1,
        ];*/
        $first = self::where('first',$user_id)->where('is_buymill',1)->count('id');
        $second = self::where('second',$user_id)->where('is_buymill',1)->count('id');
        $third = self::where('third',$user_id)->where('is_buymill',1)->count('id');

        $count = [
            'count' => $first + $second + $third,
            'directnum' => $first,
        ];
        return $count;
    }

//    public function getIsCertificationAttr($value)
//    {
//        $status = [ 0=>'未认证',1=>'已认证' ];
//        return $status[$value];
//    }

    public function getAvatarAttr($value)
    {
        return $value?$value:config('default_avatar');
    }
}