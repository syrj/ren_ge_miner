<?php
/**
 * Created by PhpStorm.
 * User: DR
 * Date: 2018/8/2
 * Time: 16:35
 */

namespace app\api\model;


use think\exception\ValidateException;
use think\Model;

class BaseModel extends Model
{
    //获取到秒的时间
    public function getDateTime($value,$data)
    {
        $value = $value ? $value : '';
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    //获取到日期的时间
    public function getDateDay($value,$data)
    {
        $value = $value ? $value : '';
        return is_numeric($value) ? date("Y-m-d", $value) : $value;
    }

    //检查cny余额
    public static function checkUserCny($user,$cny)
    {
        if($user->cny < $cny)
        {
            throw new ValidateException('cny不足!');
        }
    }

    /*
     * 收支,冻结cny
     * @param object $user 用户
     * @money double $money 收支金额
     * @freeze boolean  $freeze 是否冻结
     * @type string $type 币种 sac,cny
     */
    public static function dealUserMoney($user,$money,$freeze=false,$type='cny')
    {
        if($freeze)
        {
            if($type == 'cny' )
            {
                $user->cny -= $money;
                $user->freeze_cny += $money;
            }elseif($type == 'sac'){
                $user->sac -= $money;
                $user->freeze_sac = $user->freeze_sac + $money;
            }
        }else{
            if($type == 'cny' )
            {
                $user->cny += $money;
            }elseif($type == 'sac'){
                $user->sac += $money;
            }
        }
        $user->save();
    }

}