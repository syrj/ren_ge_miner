<?php
/**
 * Created by PhpStorm.
 * User: DR
 * Date: 2018/8/5
 * Time: 18:16
 */

namespace app\api\model;


class Order extends BaseModel
{
    public function getCreateTimeAttr($value,$data)
    {
        return $this->getDateDay($value,$data);
    }

    public function getComplateTimeAttr($value,$data)
    {
        return $this->getDateDay($value,$data);
    }

}