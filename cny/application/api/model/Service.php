<?php
/**
 * Created by PhpStorm.
 * User: DR
 * Date: 2018/8/9
 * Time: 9:37
 */

namespace app\api\model;


class Service extends  BaseModel
{
    protected $hidden = ['id','user_id','reply_id'];

    /**
     * 发送信息
     * @param $user object
     * @param $message array
     */
    public static function send($user,$message)
    {
        $data = [];
        if(isset($message['images'])){
            $data['images'] = $message['images'];
        }
        if(isset($message['content'])){
            $data['content'] = $message['content'];
        }
        $data['user_id'] = $user->id;
        $data['status'] = 0;
        $data['create_time'] = time();
        self::insert($data);
    }

    public function getCreateTimeAttr($value,$data)
    {
        return $this->getDateTime($value,$data);
    }

    public function getReplyTimeAttr($value,$data)
    {
        return $this->getDateTime($value,$data);
    }
}