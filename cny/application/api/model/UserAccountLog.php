<?php
/**
 * Created by PhpStorm.
 * User: DR
 * Date: 2018/8/5
 * Time: 11:06
 */

namespace app\api\model;


class UserAccountLog extends BaseModel
{
    protected $hidden = ['id','user_id'];

    public function getChangeTimeAttr($value,$data)
    {
        return $this->getDateTime($value,$data);
    }
}