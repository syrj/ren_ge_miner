<?php
/**
 * Created by PhpStorm.
 * User: DR
 * Date: 2018/8/2
 * Time: 19:23
 */

namespace app\api\model;


class Information extends BaseModel
{
    protected $hidden = ['id','type','status'];

    public function getCreateTimeAttr($value,$data)
    {
        return $this->getDateTime($value,$data);
    }

    public static function getInformationByType($type)
    {
        $where['type'] = $type;
        $where['status'] = 1;
        return self::where($where)->find();
    }
}