<?php

//  配置文件

// 其他模块加载 \think\Config::load(APP_PATH.'api/config.php');

return [
    'exception_handle'        => '\\app\\api\\library\\ExceptionHandle',

    'default_avatar' => '/assets/img/avatar.png',

    'multiple'  => 500,

    //  实名认证状态
    'AuthStatus'    =>  [
        'NoAuth'    =>  0,  //  未审核
        'IsAuth'    =>  1,  //  审核中
        'Pass'      =>  2,  //  审核通过
        'Fail'      =>  -1  //  审核失败
    ],

    //  是否已经认证 User 表
    'IsCert'        =>  [
        'UnCertified'   =>  0,  //  未认证
        'Certified'     =>  1,  //  已认证
    ],

    //sac接口地址
    'sac_url' => 'http://www.bishijie.com/api/coinchart/sachain',

    //  交易表类型
    'DealType'      =>  [
        'Buy'       =>  0,  //  买入
        'Sold'      =>  1,  //  卖出
    ],
    //  交易表状态
    'DealStatus'    =>  [
        'Cancel'    =>  0,  //  取消交易
        'NoDeal'    =>  1,  //  未交易
        'Dealing'   =>  2,  //  交易中
        'Finished'  =>  3   //  交易完成
    ],
    //  交易表付款状态
    'DealPayStatus' =>  [
        'NoPay'     =>  0,  //  未支付
        'NoReceipt' =>  1,  //  未收款
        'Finished'  =>  2   //  交易完成
    ],

    //  订单表状态
    'OrderStatus'   =>  [
        'NoDeal'    =>  1,  //  未交易
        'Dealing'   =>  2,  //  交易中
        'Finished'  =>  3   //  已完成
    ],

    //  矿机状态
    'MillStatus'    =>  [
        'disable'   =>  0,  //  未启用
        'enable'    =>  1,  //  启用
        'close'     =>  2   //  已结束
    ],


];
