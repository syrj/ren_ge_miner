<?php

namespace app\api\validate;

use think\Validate;

class Account extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'nickname'  =>  'require',
        'mobile'    =>  'require|length:11',
        'captcha'   =>  'require',
        'new_pay_password'  =>  'require|number|length:6',
        'new_password'  =>  'require',
        'old_phone' =>  'require|length:11',
        'old_captcha'=> 'require',
        'new_phone' =>  'require|length:11',
        'new_captcha'=> 'require',
        'real_name' =>  'require',
        'wechat' =>  'require',
        'alipay' =>  'require',
        'bank_realname' =>  'require',
        'bank_card_num' =>  'require|length:16,19',
        'bank_address'  =>  'require',
        'avatar'        =>  'require',

    ];
    /**
     * 提示消息
     */
    protected $message = [
        'nickname.require'  =>  '昵称不能为空',
        'mobile.length'     =>  '手机号长度不正确',
        'mobile.require'    =>  '手机号不能为空',
        'captcha.require'   =>  '验证码不能为空',
        'new_pay_password.require'  =>  '支付密码不能为空',
        'new_pay_password.number'  =>  '支付密码只能为数字',
        'new_pay_password.length'  =>  '支付密码长度为6',
        'new_password.require'      =>  '新密码不能为空',
        //
        'old_phone.length'          =>  '原手机号长度不正确',
        'old_phone.require'         =>  '原手机号不能为空',
        'old_captcha.require'       =>  '原手机验证码不能为空',
        'new_phone.length'          =>  '新手机号长度不正确',
        'new_phone.require'         =>  '新手机号不能为空',
        'new_captcha.require'       =>  '新手机验证码不能为空',
        //
        'real_name.require'  =>  '真实姓名不能为空',
        'wechat.require'    =>  '微信号不能为空',
        'alipay.require'    =>  '支付宝不能为空',
        'id_number.require'  =>  '身份证号不能为空',
        'id_number.alphaNum'  =>  '身份证号只能是字母或数字',
        'id_number.between'  =>  '身份证号长度不正确',
        'id_number.^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}([0-9]|X)$'  =>  '身份证号格式不正确',
        //
        'bank_address.require'  =>  '开户行不能为空',
        'bank_realname.require'  =>  '持卡人不能为空',
        'bank_card_num.require'  =>  '银行卡号不能为空',
        'bank_card_num.length'  =>  '银行卡号长度不正确',
        //
        'avatar.require'        =>  '头像地址不能为空',
    ];
    /**
     * 验证场景
     */
    protected $scene = [
        'changeNickname'    =>  ['nickname'],
        'new_pay_password'  =>  ['mobile','captcha','new_pay_password'],
        'new_password'      =>  ['mobile','new_password','captcha'],
        'change_phone'      =>  ['old_phone','old_captcha','new_phone','new_captcha'],
        'real_name_auth'    =>  ['real_name','id_number'],
        'wechat'            =>  ['wechat'],
        'alipay'            =>  ['alipay'],
        'bank'              =>  ['bank_realname','bank_card_num','bank_address'],
        'avatar'            =>  ['avatar'],
        'reset_password'    =>  ['mobile','captcha','new_password']
    ];

}
