<?php

namespace app\api\validate;

use think\Validate;

class User extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'mobile'        =>  'require|number|^1\d{10}$',
        'password'      =>  'require',
        'nickname'      =>  'require',
    ];
    /**
     * 提示消息
     */
    protected $message = [
        'mobile.require'    =>  '手机号不能为空',
        'mobile.number'     =>  '手机号格式有误',
        'password.require'  =>  '密码不能为空',
        'nickname.require'  =>  '用户姓名不能为空',
    ];
    /**
     * 验证场景
     */
    protected $scene = [
        'register'    =>  ['mobile' , 'password' , 'nickname'],

    ];

}
