<?php
/**
 * 登录api
 * @author Administrator
 */
use app\admin\model\MemberModel;
use app\admin\model\MoneyModel;
require_once(APP_PATH.'api/TestApi.php');
class LoginApi {
	/**
	 * 登录操作  用户名登录
	 * @param 会员名 $name
	 * @param 密码 $password
	 * @return array 返回登录信息
	 */
	public function doLoginByName($name,$password)
	{
		$M_Member = new MemberModel();
		//查询当前会员
		$member = $M_Member ->getMemberOne('user_name',$name);
		$pass = md5($password);
		//用户名错误	//比对密码
		if(!$member || $pass != $member->pwd){
			$json['data'] = array('uid'=>null,'token'=>null);
			$json['code']= -2;
			$json['msg']= '用户名或密码不正确';
			return $json;
		}
		//添加用户钱包
		$this ->parallelWallet($member->member_id);
		//获取token
		$token  = $this ->getToKen($member->member_id);
		$data['token'] = $token;
		//token储存
		$M_Member->updateMember($member->member_id, $data);
		//返回信息
		$json['data'] = array('uid'=>$member->member_id,'token'=>$token);
		$json['code']= 1;
		$json['msg']= '登录成功';
		return $json;
	
	}
	
	
	/**
	 * 登录操作  用户名登录 
	 * @param 会员名 $name
	 * @param 密码 $password
	 * @return array 返回登录信息
	 */
	public function doLoginByName_bak($name,$password)
	{
		$M_Member = new MemberModel();
		//查询当前会员
		$member = $M_Member ->getMemberOne('user_name',$name);
		//没有查询到就走老会员查询
		if(!$member){
			$member = $this ->getOldMember($name);
			if($member){
				$this ->addMember($member);
			}
			$member = $M_Member ->getMemberOne('user_name',$name);
		}
		$pass = md5($password);
		//用户名错误	//比对密码
		if(!$member || $pass != $member->pwd){
			$json['data'] = array('uid'=>null,'token'=>null);
			$json['code']= -2;
			$json['msg']= '用户名或密码不正确';
			return $json;
		}
		dump($member);
		//店主级别需要同步会员钱包信息
		if($member->level != 1){
			$money = $this ->getOldMoney($member->user_name);
			$M_Money = new MoneyModel();
			foreach ($money as $k=>$v){
				$re[] = $M_Money ->setMoney($member->member_id,$k,$v);
			}
			//操作成功 清空老会员资金
			if(in_array(1,$re)){
				$A_Test = new \testApi();
				$A_Test->setMoney($member->user_name);
			}
		}
		//用户为升级会员 查看老会员信息
		$data=array();
		$me = $this ->getOldMember($member->user_name);
		dump($me);
		//普通 或店主
		if($me['status']+1 > $member->level){
			$data['level'] = $me['status']+1;
		}
		//店主级别
		$shop_level = 0;
		if($me['status']==1){
			$shop_level = $me['level'];
		}
		//店主级别大当前就同步会员信息
		if($shop_level > $member->center_level){
			$data['shop_level'] = $shop_level;
		}
		//星级店主
		if($me['yunying'] > $member->center_level){
			$data['center_level'] = $me['yunying'];
		}
		//获取token
		$token  = $this ->getToKen($member->member_id);
		$data['token'] = $token;
		//token储存
		$M_Member->updateMember($member->member_id, $data);
		//返回信息
		$json['data'] = array('uid'=>$member->member_id,'token'=>$token);
		$json['code']= 1;
		$json['msg']= '登录成功';
		return $json;
	
	}
	/**
	 * 获取老会员信息
	 * @param string $name	用户名
	 * @return 会员信息
	 */
	private function getOldMember($name){
		$A_Test = new \testApi();
		$member = $A_Test ->getMemberOneByName($name);
		return $member;
	}
	/**
	 * 获取老系统资金
	 * @param string $name	会员名
	 * @return number 两种金额钱
     */
	private function getOldMoney($name){
		$A_Test = new \testApi();
		$member = $A_Test ->getMemberOneByName($name);
		//用户资金信息
		$money[1] = $member['money_jifen'];
		$money[2] = $member['money_tuijian']+$member['money_yongjin']+$member['money'];
		return $money;
	}
	/**
	 * 添加金额
	 */
	private function addMember($member){
		$test = new \TestApi();
		//推荐人信息
		$pid = $test ->getMemberOne($member['pid']);
		//拼接同步数据
		$data['user_name'] = $member['username'];
		$data['pwd'] = $member['pwd'];
		$data['pay_pwd'] = $member['spwd'];
		$data['full_name'] = $member['bank_cardname']?$member['bank_cardname']:'';
		$data['weixin'] = $member['weixin']?$member['weixin']:'';
		$data['phone'] = $member['phone'];
		$data['status'] = $member['status'];
		$data['level'] = $member['status']+1;
		$data['shop_level'] = $member['level'];
		$data['center_level'] = $member['yunying'];
		$data['tuijian_id'] = $pid['uid']?$pid['uid']:'';
		$data['tuijian_name'] = $pid['username']?$pid['username']:'';
		//用户资金信息
		$mone[0] = $member['money_jifen'];
		$mone[1] = $member['money_tuijian']+$member['money_yongjin']+$member['money'];
		//添加数据 用户信息
		$Member	= new MemberModel();
		//查询会员信息
		$re = $Member->getMemberOne('user_name',$member['username']);
		if($re){
			return true;
		}
		foreach ($data as $k=>$v){
			$Member->$k = $v;
		}
		$re = $Member->save();
		if(!$re){
			return true;
		}
		//获取新增会员id
		$member_id= $Member->member_id;
		$money = new MoneyModel();
		//钱包信息
		$currency = db('Currency')->where(array('status'=>1))->select();
		//添加钱包信息
		foreach ($currency as $k=>$v){
			$list[] = ['member_id'=>$member_id,'currency_id'=>$v['currency_id'],'money'=>$mone[$k]];
		}
		$money->saveAll($list);
		$test ->setMoney($member['username']);
		return true;
	}
	
	/**
	 * 同步钱包信息
	 * @param int $member_id 用户id
	 */
	public function parallelWallet($member_id){
		$M_Money = new MoneyModel();
		$money_array = array();
		//获取钱包数据
		$currency = db('Currency')->field('currency_id')->where(array('status'=>1))->select();
		foreach ($currency as $k=>$v){
			$curr_array[$k]=$v['currency_id'];
			$money_where=array(
					'member_id'=>$member_id,
					'currency_id'=>$v['currency_id']
			);
			$money = $M_Money->where($money_where)->find();
			if($money){
				$money_array[$k]=$v['currency_id'];
			}
		}
		//去除相同钱包
		if($money_array){
			$curr_array = array_diff($curr_array,$money_array);
		}
		//为空返回数据
		if(empty($curr_array)){
			return true;
		}
		foreach ($curr_array as $k=>$v){
			$list[] = ['member_id'=>$member_id,'currency_id'=>$v,'money'=>0];
		}
		$re = $M_Money->saveAll($list);
		return $re?$re:false;
	}
	
	/**
	 * 获取token
	 * @param int $member_id	会员id
	 * @return string token
	 */
	private  function getToKen($member_id){
		return md5($member_id.time());
	}
}