<?php
/**
 * @description  	银行卡验证
 * @author      	LiYI <1041012846@qq.com>
 * @date 2016-11-1 	时间
 * @version 1.0.0	版本
 * @copyright
 */
header('Content-type:text/html;charset=utf-8');
class BankApi {
	//配置您申请的key
	public $appkey = "eec412151bb04e8db96492e1b64b011f";
	/**
	 * 构造函数
	 * @param string $key			应用APPKEY
	 */
	function __construct($appkey=''){
		//修改默认值
		$this ->appkey		= $appkey?$appkey:$this ->appkey;
	}
	/**
	 */
	Public function checkBank($cardnum ='6222021001076053606'){
		$smsapi = "http://api.avatardata.cn/Bank/Query";
		$sendurl = $smsapi.'?key='.$this->appkey.'&cardnum='.$cardnum;
		$sendurl=urldecode($sendurl);
		//$result =file_get_contents($sendurl);
		$result='{"result":{"cardtype":"借记卡","cardlength":19,"cardprefixnum":"622202","cardname":"E时代卡","bankname":"工商银行","banknum":"1020000"},"error_code":0,"reason":"Succes"}';
		return $result;
		//$result = json_decode($result);
	}
	
}