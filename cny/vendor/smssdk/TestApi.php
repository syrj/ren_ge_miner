<?php
class testApi {
	public $link;
	function __construct(){
		$this ->linkDb();
	}
	public function getMember($k){
		$sql = "SELECT * FROM  yljz_user limit {$k},1";
		return $this ->lianKu($sql);
	}
	/**
	 * 通过id 获取用户信息
	 * @param int 	$id		用户id
	 * @return multitype:
	 */
	public function getMemberOne($id){
		$sql = "SELECT * FROM  yljz_user where uid='{$id}'";
		return $this ->lianKu($sql);
	}
	/**
	 * 根据用户名获取用户信息
	 * @param status $name 用户名
	 * @return multitype:
	 */
	public function getMemberOneByName($name){
		$sql = "SELECT * FROM  yljz_user where username='{$name}'";
		return $this ->lianKu($sql);
	}
	/**
	 * 清空会员资金
	 * @param status $name 用户名
	 * @return multitype:
	 */
	public function setMoney($name){
		$sql = "UPDATE yljz_user SET money = 0,money_jifen = 0,money_tuijian = 0,money_yongjin = 0 where username = '{$name}'";
		return $this ->sqlCarry($sql);
	}
	
	/**
	 * 更新用户信息
	 * @param status $name 用户名
	 */
	public function updateMemberStatus($name){
		$sql = "UPDATE yljz_user SET status = 1 where username = '{$name}'";
		return $this ->sqlCarry($sql);
	}
	
	
	/**
	 * 添加用户
	 * @param unknown $name
	 * @param unknown $pwd
	 * @param unknown $spwd
	 * @param unknown $phone
	 * @param unknown $weixin
	 * @param unknown $tuiijian   推荐人姓名
	 */
	public function addMember($name, $pwd, $spwd, $phone, $tuiijian,$weixin) {
		$pid = '';
		if(!empty($tuiijian)){
			$member = $this->getMemberOneByName($tuiijian);
			if($member){
				$pid = $member['uid'];
			}
		}
		$pwd = md5($pwd);
		$spwd = md5($spwd);
		$time = time();
		$sql = "insert into  yljz_user 
				(username,pwd,spwd,phone,weixin,reg_time,pid) 
				values('$name','$pwd','$spwd','$phone','$weixin','$time','$pid')";
		return $this->sqlCarry ( $sql );
	}
	
	/**
	 * 链接数据库
	 */
	private function linkDb(){
		$db_host="112.74.43.216";		//数据库地址
		$db_user="root";			//数据库用户名
		$db_pass="root";			//数据库密码
		$db_name="hjsj";//数据库表
	//	$this ->link=mysqli_connect($db_host,$db_user,$db_pass,$db_name);	//连接数据库
		// mysql_select_db($db_name,$link);					//选择库
		// mysqli_query("SET names UTF8");						//设置字符集
		$this ->link = mysqli_connect(
				$db_host,  /* The host to connect to 连接MySQL地址 */
				$db_user,      /* The user to connect as 连接MySQL用户名 */
				$db_pass,  /* The password to use 连接MySQL密码 */
				$db_name);    /* The default database to query 连接数据库名称*/
		
		if (!$this ->link) {
			printf("Can't connect to MySQL Server. Errorcode: %s ", mysqli_connect_error());
			exit;
		}
		
	}
	//执行语句
	protected function sqlCarry($sql){
		$result=mysqli_query($this ->link,$sql);
	}
	//返回一个结果集 只查一个的时候用
	protected function lianKu($sql){
		$result=mysqli_query($this ->link, $sql);
		$user=mysqli_fetch_assoc($result);
		mysqli_free_result($result);
		return $user;
	}
	//返回一个结果集 查多个的时候用
	protected  function lianKuArray($sql){
		$res="";
		$result=mysqli_query($this ->link,$sql);
		while($user=mysqli_fetch_assoc($result)){
			$res[]=$user;
		}
		mysqli_free_result($result);
		return $res;
	}
	
}