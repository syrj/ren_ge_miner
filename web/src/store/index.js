import Vue from 'vue'
import Vuex from 'vuex'
import vuexI18n from 'vuex-i18n'
import actions from './actions'
import mutations from './mutations'
import getters from './getters'

Vue.use(Vuex)

const state = {
  isLoading: false,
  isCollapse: false,
  userInfo: null,
  needValidate: false
}

export default new Vuex.Store({
  state,
  actions,
  getters,
  mutations,
  modules: {
    i18n: vuexI18n.store
  }
})
