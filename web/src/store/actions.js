export default {
  updateCollapse ({ commit, state }, payload) {
    commit('UPDATE_COLLAPSE', payload)
  },
  updateHeaderTitle ({ commit, state }, payload) {
    commit('UPDATE_HEADERTITLE', payload)
  },
  updateLoading ({ commit, state }, payload) {
    commit('UPDATE_LOADING', payload)
  },
  updateValitdate ({ commit, state }, payload) {
    commit('UPDATE_VALIDATE', payload)
  }
}
