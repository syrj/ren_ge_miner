import objectAssign from 'object-assign'
import Vue from 'vue'
import FastClick from 'fastclick'
import router from './router'
import store from './store'
import App from './App'
// import Vuex from 'vuex'
import vuexI18n from 'vuex-i18n'
import vuxLocales from './locales/all.yml'
import componentsLocales from './locales/components.yml'
import { AjaxPlugin, AlertPlugin, ToastPlugin, LoadingPlugin, TransferDom, LocalePlugin, ConfirmPlugin } from 'vux'
import VueScroller from 'vue-scroller'
Vue.use(VueScroller)

// Vue.use(Vuex)
// /** i18n **/
// let store = new Vuex.Store({
//   modules: {
//     i18n: vuexI18n.store
//   }
// })

Vue.use(vuexI18n.plugin, store)

const finalLocales = {
  'en': objectAssign(vuxLocales['en'], componentsLocales['en']),
  'zh-CN': objectAssign(vuxLocales['zh-CN'], componentsLocales['zh-CN'])
}

for (let i in finalLocales) {
  Vue.i18n.add(i, finalLocales[i])
}

Vue.directive('transfer-dom', TransferDom)
Vue.use(AlertPlugin)
Vue.use(ToastPlugin, {width: '13.6em'})
Vue.use(LoadingPlugin)
Vue.use(AjaxPlugin)
Vue.use(LocalePlugin)
Vue.use(ConfirmPlugin)

// const nowLocale = Vue.locale.get()
// if (/zh/.test(nowLocale)) {
//   Vue.i18n.set('zh-CN')
// } else {
//   Vue.i18n.set('en')
// }
if (localStorage.language === 'zh-CN') {
  Vue.i18n.set('zh-CN')
} else {
  Vue.i18n.set('en')
}

FastClick.attach(document.body)

Vue.prototype.$baseUrl = ''
// Vue.prototype.$baseUrl = 'http://cny.com'

const apiPrefix = ''
// const loginTimeOutErrorCode = 'login_timeout_error'

Vue.prototype.http = function (opts) {
  let vue = this
  if (opts.showloading === undefined) {
    opts.showloading = true
  }
  if (opts.showloading) {
    vue.$vux.loading.show({
      text: '加载中...'
    })
  }
  vue.$http({
    method: opts.method,
    url: apiPrefix + opts.url,
    headers: opts.headers || {},
    params: opts.params || {},
    data: opts.data || {}
  }).then(function (response) {
    vue.$vux.loading.hide()
    opts.success(response.data)
  }).catch(function (error) {
    vue.$vux.loading.hide()
    // console.log(error.response)
    if (!opts.error) {
      let response = error.response
      let errorMessage = '请求失败'
      if (response && response.data) {
        if (response.data.code === 401) {
          vue.$vux.alert.show({
            title: vue.$t('info'),
            content: vue.$t('need login'),
            onHide () {
              vue.$router.push({path: '/'})
            }
          })
        } else {
          if (response.data.msg) {
            errorMessage = response.data.msg
          } else {
            errorMessage = response.status + ' ' + response.statusText
          }
          vue.$vux.alert.show({
            title: '提示',
            content: errorMessage
          })
        }
      }
    } else {
      opts.error(error.response.data)
    }
  })
}

Vue.prototype.get = function (opts) {
  opts.method = 'get'
  this.http(opts)
}

Vue.prototype.post = function (opts) {
  opts.method = 'post'
  this.http(opts)
}

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  next()
})

/* eslint-disable no-new */
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app-box')
