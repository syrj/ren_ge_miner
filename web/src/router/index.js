import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloFromVux'
import Index from '../view/'
import NotFound from '../view/notfound'
// 登陆&注册
import Login from '../view/account/login'
import Agreement from '../view/account/agreement'
import Forgot from '../view/account/forgot'
import Resgister from '../view/account/register'
// 矿机
import Mill from '../view/mill/Mill'
import Mill2 from '../view/mill/Mill2'
// 交易
import transaction from '../view/transaction/'
// 个人中心
import My from '../view/personal/'
import PersonalInfo from '../view/personal/personalInfo'
import editNickname from '../view/personal/editNickName'
import validateFir from '../view/personal/validateFir'
import validateSec from '../view/personal/validateSec'
import myMine from '../view/personal/myMine'
import editWechat from '../view/personal/editWechat'
import editAlipay from '../view/personal/editAlipay'
import editCard from '../view/personal/editCard.vue'
// 我的买单
import tradeList from '../view/personal/tradelist'
import tradePayCoin from '../view/personal/tradePayCoin'
import netCoin from '../view/personal/netcoin'
import serviceCenter from '../view/personal/serviceCenter'
import accountCenter from '../view/personal/accountCenter'
import editAccount from '../view/personal/editAccount'
import payFirst from '../view/personal/payFirst'
import payCode from '../view/personal/payCode'
import payThird from '../view/personal/payThird'
import moneyAccount from '../view/personal/moneyAccount'
import payAccount from '../view/personal/payAccount'
import myCode from '../view/personal/myCode'
import myTeam from '../view/personal/myTeam'
import qqQun from '../view/personal/qqQun'
import saftyCenter from '../view/personal/saftyCenter'
import editPwd from '../view/personal/editPwd'
import editTradePwd from '../view/personal/editTradePwd'
import editPhone from '../view/personal/editPhone'
import NoticeList from '../view/personal/noticeList'
import Ondev from '../view/personal/ondev'
import aboutUs from '../view/personal/aboutus'
// 公告
// import Notice from '../view/notice/'
import NoticeDetail from '../view/notice/detail'
// 资讯
import News from '../view/news/'
import NewsDetail from '../view/news/detail'
// 财务日志
import CashLog from '../view/CashLog'
// 我的挂单
import EntryOrders from '../view/Entry_Orders'
// 推广
import Generalize from '../view/Generalize'
// 我的矿机
import Master from '../view/Master'
// CNY
import CNY from '../view/CNY'
// SAC
import SAC from '../view/SAC'
// 转账
import Transfer from '../view/Transfer'
// 我的挂单
import MyEntryOrders from '../view/MyEntryOrders'
// 聊天
import chartingRoom from '../view/chart/'
import Feedback from '../view/chart/feedback'

Vue.use(Router)
export default new Router({
  routes: [
    {
      path: '/SAC',
      name: 'SAC',
      component: SAC
    },
    {
      path: '/CNY',
      name: 'CNY',
      component: CNY
    },
    {
      path: '/MyEntryOrders',
      name: 'myorder',
      component: MyEntryOrders
    },
    {
      path: '/index',
      name: 'index',
      component: Index
    },
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/agreement',
      name: 'agreement',
      component: Agreement
    },
    {
      path: '/forgot',
      name: 'reset',
      component: Forgot
    },
    {
      path: '/register',
      name: 'register',
      component: Resgister
    },
    {
      path: '/mill',
      name: 'mill',
      component: Mill
    },
    {
      path: '/mill2',
      name: 'mill2',
      component: Mill2
    },
    {
      path: '/transaction',
      name: 'trade',
      component: transaction
    },
    {
      path: '/noticedetail',
      name: '公告详情',
      component: NoticeDetail
    },
    {
      path: '/news',
      name: '资讯',
      component: News
    },
    {
      path: '/newsdetail',
      name: '资讯详情',
      component: NewsDetail
    },
    {
      path: '/personal',
      name: 'mine',
      component: My
    },
    {
      path: '/personalInfo',
      name: 'personalInfo',
      component: PersonalInfo
    },
    {
      path: '/editnickname',
      name: 'editnickname',
      component: editNickname
    },
    {
      path: '/editWechat',
      name: 'editWechat',
      component: editWechat
    },
    {
      path: '/editAlipay',
      name: 'editAlipay',
      component: editAlipay
    },
    {
      path: '/editCard',
      name: 'editCard',
      component: editCard
    },
    {
      path: '/validatefir',
      name: 'validatefir',
      component: validateFir
    },
    {
      path: '/validatesec',
      name: 'validatesec',
      component: validateSec
    },
    {
      path: '/mymine',
      name: 'mymine',
      component: myMine
    },
    {
      path: '/tradelist',
      name: 'tradelist',
      component: tradeList
    },
    {
      path: '/tradepaycoin',
      name: 'tradepaycoin',
      component: tradePayCoin
    },
    {
      path: '/netcoin',
      name: '云币明细',
      component: netCoin
    },
    {
      path: '/servicecenter',
      name: 'servicecenter',
      component: serviceCenter
    },
    {
      path: '/account',
      name: 'account',
      component: accountCenter
    },
    {
      path: '/payaccount',
      name: 'payaccount',
      component: payAccount
    },
    {
      path: '/editaccount',
      name: 'editaccount',
      component: editAccount
    },
    {
      path: '/moneyaccount',
      name: 'moneyaccount',
      component: moneyAccount
    },
    {
      path: '/payfirst',
      name: 'payfirst',
      component: payFirst
    },
    {
      path: '/paycode',
      name: 'paycode',
      component: payCode
    },
    {
      path: '/paythird',
      name: 'paythird',
      component: payThird
    },
    {
      path: '/mycode',
      name: 'mycode',
      component: myCode
    },
    {
      path: '/myteam',
      name: 'myteam',
      component: myTeam
    },
    {
      path: '/aboutus',
      name: 'aboutus',
      component: aboutUs
    },
    {
      path: '/qqqun',
      name: 'qqqun',
      component: qqQun
    },
    {
      path: '/safty',
      name: 'safty',
      component: saftyCenter
    },
    {
      path: '/editphone',
      name: 'editphone',
      component: editPhone
    },
    {
      path: '/editpwd',
      name: 'editpwd',
      component: editPwd
    },
    {
      path: '/edittradepwd',
      name: 'edittradepwd',
      component: editTradePwd
    },
    {
      path: '/noticelist',
      name: '公告中心',
      component: NoticeList
    },
    {
      path: '/ondev',
      name: '开发中',
      component: Ondev
    },
    {
      path: '/Entry_Orders',
      name: '我的挂单',
      component: EntryOrders
    },
    {
      path: '/cashLog',
      name: 'cashLog',
      component: CashLog
    },
    {
      path: '/Generalize',
      name: '推广',
      component: Generalize
    },
    {
      path: '/master',
      name: 'myminer',
      component: Master
    },
    {
      path: '/transfer',
      name: 'transfer',
      component: Transfer
    },
    {
      path: '/charting',
      name: 'charting',
      component: chartingRoom
    },
    {
      path: '/feedback',
      name: 'feedback',
      component: Feedback
    },
    {
      path: '*',
      name: '404',
      component: NotFound
    }
  ]
})
